/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author madar
 */
public class Cola<T> {

    private ListaCD<T> tope = new ListaCD();

    public Cola() {
    }

    public void enColar(T info) {
        this.tope.insertarFin(info);
    }

    public T deColar() {
        return this.tope.eliminar(0);
    }

    public int getTamanio() {
        return this.tope.getTamanio();
    }

    public boolean esVacia() {
        return this.getTamanio() == 0;
    }
}
