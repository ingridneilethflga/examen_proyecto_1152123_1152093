/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Negocio.Sistema_ColombiaAprende;

/**
 *
 * @author INGRID NEILETH FLOREZ GARAY - 1152123
 *         KEVIN FRAYMAN ROMERO CUELLAR - 1152093
 */
public class Test {

    public static void main(String[] args) {
        Sistema_ColombiaAprende sis = new Sistema_ColombiaAprende();
        String url1 = "https://gitlab.com/pruebas_madarme/persistencia/ed_a/colombia_aprende/-/raw/main/personas_computadores.csv";
        String url2 = "https://gitlab.com/pruebas_madarme/persistencia/ed_a/colombia_aprende/-/raw/main/hardware.csv";

        sis.cargarDatos(url1, url2, true);

        System.out.println(sis.toString());
    }
}
