/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Hardware_Academico;
import Modelo.Notificacion;
import Modelo.Usuario;
import java.time.LocalTime;
import ufps.util.colecciones_seed.ColaP;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Pila;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author INGRID NEILETH FLOREZ GARAY - 1152123 
 * KEVIN FRAYMAN ROMERO CUELLAR - 1152093
 */
public class Sistema_ColombiaAprende {

    private Pila<Hardware_Academico> inventario[];
    private ColaP<Usuario> usuarios;
    private ListaCD<Notificacion> notificaciones;

    public Sistema_ColombiaAprende() {
        inventario = new Pila[2];
        inventario[0] = new Pila();
        inventario[1] = new Pila();
        usuarios = new ColaP();
        notificaciones = new ListaCD();
    }

    /**
     * se cargan los dartos de los registros
     *
     * @param url1 url de usuarios
     * @param url2 url de hardware
     * @param tipo tipo de notificacion
     */
    public void cargarDatos(String url1, String url2, boolean tipo) {
        this.registarUsuarios(url1);
        this.registrarHardware(url2);
        this.asignarHardware(tipo);
    }

    public Pila<Hardware_Academico>[] getInventario() {
        return inventario;
    }

    public ColaP<Usuario> getUsuarios() {
        return usuarios;
    }

    public ListaCD<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    /**
     * Metodo para registrar los usuarios de la csv
     *
     * @param ur url del csv
     */
    private void registarUsuarios(String ur) {
        ArchivoLeerURL ur1 = new ArchivoLeerURL(ur);
        Object[] datos = ur1.leerArchivo();

        for (int i = 1; i < datos.length; i++) {

            String datoFila = datos[i].toString();
            String[] datosUsuario = datoFila.split(";");

            int cedula = Integer.valueOf(datosUsuario[0]);
            String nombres = datosUsuario[1];
            String email = datosUsuario[2];
            String horaRegistro = datosUsuario[3];

            horaRegistro = validarHora(horaRegistro);

            byte cantInfantes = Byte.valueOf(datosUsuario[4]);
            byte cantAdolescentes = Byte.valueOf(datosUsuario[5]);

            Usuario s = new Usuario(cedula, nombres, email, LocalTime.parse(horaRegistro), cantInfantes, cantAdolescentes);
            usuarios.enColar(s, s.prioridad(horaRegistro) * -1);
        }
    }

    /**
     * Metodo para registrar el Hardware de la csv
     *
     * @param ur url del csv
     */
    private void registrarHardware(String ur) {
        ArchivoLeerURL ur1 = new ArchivoLeerURL(ur);
        Object[] datos = ur1.leerArchivo();
        for (int i = 1; i < datos.length; i++) {
            String datoFila = datos[i].toString();
            String[] datosUsuario = datoFila.split(";");
            int id = Integer.valueOf(datosUsuario[0]);
            boolean tipo = Boolean.valueOf(datosUsuario[1]);
            String descripcion = datosUsuario[2];

            Hardware_Academico ha = new Hardware_Academico(tipo, id, descripcion);
            if (ha.isTipo()) {
                this.inventario[0].push(ha);
            } else {
                this.inventario[1].push(ha);
            }
        }
    }

    /**
     * metodo que valida la hora
     *
     * @param hora
     * @return una hora
     */
    private String validarHora(String hora) {
        String[] horas = hora.split(":");
        for (int i = 0; i < horas.length; i++) {
            if (horas.length < 3) {
                hora = hora + ":00";
                return hora;
            }
            if (horas[i].length() < 2) {
                horas[i] = "0" + horas[i];
            }
        }

        if (horas.length == 3) {
            return horas[0] + ":" + horas[1] + ":" + horas[2];
        }

        return horas[0] + ":" + horas[1];
    }

    public int tamUsuarios() {
        return this.usuarios.getTamanio();
    }

    /**
     * metodo para asignar las tablets y computadores
     *
     * @param tipo tipo de notificacion
     */
    private void asignarHardware(boolean tipo) {
        ColaP<Usuario> usuarios2 = this.usuarios.clonar();
        int tam = usuarios2.getTamanio();

        for (int i = 0; i < tam; i++) {
            Usuario usu = usuarios2.deColar();
            byte tablet = 0;
            for (byte j = 0; j < usu.getCantInfantes() && !this.inventario[1].esVacia(); j++) {
                tablet++;
                this.inventario[1].pop();
            }

            byte compu = 0;
            byte H = (byte) (usu.getCantAdolescentes());
            byte compu2 = 0;
            if (H % 2 == 0) {
                compu2 = (byte) (H / 2);
            } else if (H != 1) {
                compu2 = (byte) ((H / 2) + 1);
            }

            for (byte k = 0; k < compu2 && !this.inventario[0].esVacia(); k++) {
                compu++;
                this.inventario[0].pop();
            }
            notificaciones.insertarFin(new Notificacion(usu, compu, tablet, tipo));
        }
    }

    @Override
    public String toString() {
        String r = "";

        for (Notificacion n : this.getNotificaciones()) {
            Usuario u = this.usuarios.deColar();
            r += u.toString() + n.toString() + "\n";
        }
        return r;
    }

}
